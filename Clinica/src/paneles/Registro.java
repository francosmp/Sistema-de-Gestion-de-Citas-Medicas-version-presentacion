package paneles;
import java.awt.CardLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import complementos.Conexion;
import informacion.Medico;
import informacion.Recepcionista;
public class Registro extends JPanel implements ActionListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	ButtonGroup bg;
	public String tipo;
	private JPanel panel;
	private JRadioButton rdbtnMedico;
	private JRadioButton rdbtnRecepcionista;
	private JPanel panel_1;
	private JPanel panel_2;
	RMedico rm;
	RRecep rec;
	private JButton btnRegistrar;
	public Registro(){
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{450, 0};
		gridBagLayout.rowHeights = new int[]{30, 200, 30, 0};
		gridBagLayout.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		add(panel, gbc_panel);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		rdbtnMedico = new JRadioButton("Medico");
		rdbtnMedico.setSelected(true);
		rdbtnMedico.addActionListener(this);
		panel.add(rdbtnMedico);
		
		rdbtnRecepcionista = new JRadioButton("Recepcionista");
		rdbtnRecepcionista.addActionListener(this);
		panel.add(rdbtnRecepcionista);
		
		btnRegistrar = new JButton("Registrar");
		btnRegistrar.addActionListener(this);
		panel.add(btnRegistrar);
		
		panel_1 = new JPanel();
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.insets = new Insets(0, 0, 5, 0);
		gbc_panel_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1.gridx = 0;
		gbc_panel_1.gridy = 1;
		add(panel_1, gbc_panel_1);
		
		panel_2 = new JPanel();
		panel_1.add(panel_2);
		panel_2.setLayout(new CardLayout(0, 0));
		rm=new RMedico();
		rec=new RRecep();
		panel_2.add(rm,"Medico");
		panel_2.add(rec,"Recepcionista");;
	}
	public boolean verificar(String dni){
		Connection cn=Conexion.getConexionMYSQL();
		try {
			Statement state = cn.createStatement();
			ResultSet rs=state.executeQuery("SELECT dni FROM persona where dni='"+dni+"';");
			if(rs.next()) return false;
			else return true;
			
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
			JOptionPane.showMessageDialog(null,"Fallo en la verificacion de duplicado");
			return false;
		}
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource().equals(rdbtnMedico)){
			if(rdbtnMedico.isSelected()){
			((CardLayout)panel_2.getLayout()).show(panel_2,"Medico");
			rdbtnRecepcionista.setSelected(false);
			}
		}
		if(e.getSource().equals(rdbtnRecepcionista)){
			if(rdbtnRecepcionista.isSelected()){
				((CardLayout)panel_2.getLayout()).show(panel_2,"Recepcionista");
				rdbtnMedico.setSelected(false);
				}
		}
		if(e.getSource().equals(btnRegistrar)){
			Connection cn=Conexion.getConexionMYSQL();
			if(rdbtnRecepcionista.isSelected()){
				if(!rec.isEmpty()){
				if(JOptionPane.showConfirmDialog(null,"¿Desea registrar?")==0){
				rec.llenarDatos();
				Recepcionista p=rec.getP();
				if(verificar(p.getDni())){
				try {
					Statement st=cn.createStatement();
					Statement st1=cn.createStatement();
					st.executeUpdate("INSERT INTO persona (codigo,nombres,apellidos,edad,sexo,fechaNac,telefono,nacionalidad,direccion,estadoCivil,ocupacion,dni)"
							+ " VALUES ('"+p.getCodigop()+"','"+p.getNombre()+"','"+p.getApellido()+"','"+
							String.valueOf(p.getEdad())+"','"+p.getSexo()+"','"+p.getFechaNac().toString()+"','"+p.getTelefono()+"','"+p.getNacionalidad()+
							"','"+p.getDireccion()+"','"+p.getEstadoCivil()+"','"+p.getOcupacion()+"','"+p.getDni()+"');");
					st1.executeUpdate("INSERT INTO areas (codigo,areaAtencion) values ('"+p.getCodigop()+"','"+p.getAreaAtencion()+"');");
					st1.close();
					st.close();
					cn.close();
					JOptionPane.showMessageDialog(null,"Registro exitoso");
					rec.nuevoIngreso();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					JOptionPane.showMessageDialog(null,"Fallo en el registro del Recepcionista");
				}
				}
				else JOptionPane.showMessageDialog(null,"ya existe una personas registrada con el mismo DNI");
				}
			}
				else JOptionPane.showMessageDialog(null,"Uno o más campos están vacios");
		}
		if(rdbtnMedico.isSelected()){
				if(JOptionPane.showConfirmDialog(null,"¿Desea registrar?")==0){
					if(!rm.isEmpty()){
					this.rm.llenar();
					Medico m= rm.getMedico();
					if(verificar(m.getDni())){
					try {
						Statement st=cn.createStatement();
						Statement st1=cn.createStatement();
						st.executeUpdate("INSERT INTO persona (codigo,nombres,apellidos,edad,sexo,fechaNac,telefono,nacionalidad,direccion,estadoCivil,ocupacion,dni)"
								+ " VALUES ('"+m.getCodigoM()+"','"+m.getNombre()+"','"+m.getApellido()+"','"+
								String.valueOf(m.getEdad())+"','"+m.getSexo()+"','"+"0000-00-00"+"','"+m.getTelefono()+"','"+m.getNacionalidad()+
								"','"+m.getDireccion()+"','"+m.getEstadoCivil()+"','"+m.getOcupacion()+"','"+m.getDni()+"');");
						st1.executeUpdate("INSERT INTO medicos (codigoMedico,numeroConsultorio,especialidad,dias,horaInicio,horaFin)"+" VALUES ('"+m.getCodigoM()+"','"+m.getNumConsultorio()+"','"+
								m.getEspecialidad()+"','"+m.getDias()+"','"+m.getHoraInicio()+"','"+m.getHoraFin()+"');");
						st1.close();
						st.close();
						cn.close();
						JOptionPane.showMessageDialog(null,"Registro exitoso");
						rm.nuevoIngreso();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
						JOptionPane.showMessageDialog(null,"Fallo en el registro del Medico");
					}
					}
					else JOptionPane.showMessageDialog(null,"Ya existe una personas registrada con el mismo DNI");
				}
					else JOptionPane.showMessageDialog(null,"Uno o más campos están vacios");
				}
		}
	}
	}
}
