package informacion;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import complementos.Conexion;
import complementos.registrarBasedatos;
import listas.ListaMedico;

public class Cita {

    private String codigo;
    private Medico medico;
    private Date fechaCita;
    private Time horaCita;
    private String Acceso;
    private Persona paciente;
    private String descripcion;

    public Cita() {
        paciente = new Persona();
        medico = new Medico();
    }

    public Cita(String codigo, Medico medico, Date fechaCita, Time horaCita, String descripcion) {
        super();
        this.codigo = codigo;
        this.medico = medico;
        this.fechaCita = fechaCita;
        this.horaCita = horaCita;
        this.descripcion = descripcion;
    }

    public Cita(String codigo, Medico medico, Date fechaCita, Time horaCita, Persona paciente, String descripcion) {
        super();
        this.codigo = codigo;
        this.medico = medico;
        this.fechaCita = fechaCita;
        this.paciente = paciente;
        this.horaCita = horaCita;
        this.descripcion = descripcion;
    }

    public void crearCodigo() {
        Connection cn = Conexion.getConexionMYSQL();
        int contador = 0;
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery("SELECT codigoCita FROM citas");
            while (rs.next()) {
                contador++;
            }
            String codigo = "CI" + String.valueOf(contador + 1);
            this.codigo = codigo;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public Cita Buscar(String dni) {
        Cita citas = null;
        ListaMedico lm = new ListaMedico();
        Connection conectar = Conexion.getConexionMYSQL();
        try {
            PreparedStatement ps = conectar.prepareStatement("select* from citas inner join persona on citas.codigoPaciente=persona.codigo  where persona.dni=? ");
            ps.setString(1, dni);
            ResultSet rst = ps.executeQuery();
            while (rst.next()) {
                if (!rst.getString(7).equals("Cita cancelada")) {
                    citas = new Cita();
                    citas.setCodigo(rst.getString(1));
                    citas.getPaciente().setCodigop(rst.getString(2));
                    String codigoMedico = rst.getString(3);
                    citas.setMedico(lm.buscarMedico(codigoMedico));
                    citas.setFechaCita(rst.getDate(4));
                    citas.setHoraCita(rst.getTime(5));
                    citas.setAcceso(rst.getString(6));
                    citas.setDescripcion(rst.getString(7));
                    return citas;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(registrarBasedatos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return null;
    }

    public Persona BuscarPaciente(String codigo) {
        Persona p = null;
        Connection conectar = Conexion.getConexionMYSQL();
        try {
            PreparedStatement ps = conectar.prepareStatement("select * from persona where dni=? ");
            ps.setString(1, codigo);
            ResultSet rst = ps.executeQuery();
            if (rst.next()) {
                p = new Persona();
                p.setCodigop(rst.getString(1));
                p.setNombre(rst.getString(2));
                p.setApellido(rst.getString(3));
                p.setEdad(rst.getInt(4));
                p.setSexo(rst.getString(5));
                p.setFechaNac(rst.getDate(6));
                p.setTelefono(rst.getString(7));
                p.setNacionalidad(rst.getString(8));
                p.setDireccion(rst.getString(9));
                p.setEstadoCivil(rst.getString(10));
                p.setOcupacion(rst.getString(11));
                p.setDni(rst.getString(12));
                return p;
            }

        } catch (SQLException ex) {
            Logger.getLogger(registrarBasedatos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return null;
    }

    public void crearAcceso() {
        Random rn = new Random();
        String acceso = "";
        for (int i = 0; i < 5; i++) {
            acceso += String.valueOf((char) (90 - rn.nextInt(15)));
        }
        this.Acceso = acceso;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Date getFechaCita() {
        return fechaCita;
    }

    public void setFechaCita(Date fechaCita) {
        this.fechaCita = fechaCita;
    }

    public Medico getMedico() {
        return this.medico;
    }

    public void setMedico(Medico codigoMedico) {
        this.medico = codigoMedico;
    }

    public String getAcceso() {
        return Acceso;
    }

    public void setAcceso(String acceso) {
        Acceso = acceso;
    }

    public Time getHoraCita() {
        return horaCita;
    }

    public void setHoraCita(Time horaCita) {
        this.horaCita = horaCita;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Persona getPaciente() {
        return paciente;
    }

    public void setPaciente(Persona paciente) {
        this.paciente = paciente;
    }
}

