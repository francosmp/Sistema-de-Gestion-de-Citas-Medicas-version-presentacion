
package frames;

import informacion.Cita;
import informacion.Medico;
import informacion.Persona;
import complementos.registrarBasedatos;
import informacion.Horario;
import java.awt.Color;
import java.sql.Time;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import listas.ListaMedico;

public class RegistrarPaciente extends javax.swing.JFrame {

    /**
	 * 
	 */
	private static final long serialVersionUID = 7264174231684513963L;
	Persona p = null;
    Cita cita = null;
    Medico med = null;
    registrarBasedatos rbd;
    ArrayList<Medico> lista;
    String codigoHora;
    String hora;
    public RegistrarPaciente() {
        med = new Medico();
        initComponents();
        labelHora.setVisible(false);
        cboHorasTotales.setVisible(false);
        labelResultadoCalendario.setVisible(false);
        cboEspecialidades();
        btnGuardarCita.setVisible(false);
        this.setVisible(true);
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jTabbedPaneRegistro = new javax.swing.JTabbedPane();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        jInternalFrame2 = new javax.swing.JInternalFrame();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        rboMasculino = new javax.swing.JRadioButton();
        txtApellido = new javax.swing.JTextField();
        txtnombre = new javax.swing.JTextField();
        rboFemenino = new javax.swing.JRadioButton();
        jLabel6 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        spEdad = new javax.swing.JSpinner();
        txtDni = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txtDireccion = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txtocupacion = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txtcelular = new javax.swing.JTextField();
        btnguardarPaciente = new javax.swing.JButton();
        jLabel12 = new javax.swing.JLabel();
        txtNacionalidad = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        txtCodigoPaciente = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        txtEstadoCivil = new javax.swing.JTextField();
        calendario = new com.toedter.calendar.JDateChooser();
        jLabel23 = new javax.swing.JLabel();
        txtTipoSangre = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        txtTalla = new javax.swing.JTextField();
        jLabel28 = new javax.swing.JLabel();
        txtPeso = new javax.swing.JTextField();
        jLabel29 = new javax.swing.JLabel();
        txtAlergias = new javax.swing.JTextField();
        jInternalFrame3 = new javax.swing.JInternalFrame();
        jLabeldatosPaciente = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtDescripcionCita = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        cboEspecialidades = new javax.swing.JComboBox<>();
        jLabel18 = new javax.swing.JLabel();
        cboProfesional = new javax.swing.JComboBox<>();
        btnBorrarCampos = new javax.swing.JButton();
        btnGuardarCita = new javax.swing.JButton();
        calendarioCita = new com.toedter.calendar.JDateChooser();
        jLabel7 = new javax.swing.JLabel();
        txtSala = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        jLabelCodigo = new javax.swing.JLabel();
        labelResultadoCalendario = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        labelHora = new javax.swing.JLabel();
        btnDisponibiidad = new javax.swing.JButton();
        cboHorasTotales = new javax.swing.JComboBox<>();
        jInternalFrame1 = new javax.swing.JInternalFrame();
        jLabel15 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        txtNombresPP = new javax.swing.JTextField();
        txtApellidosPP = new javax.swing.JTextField();
        txtEdadPP = new javax.swing.JTextField();
        txtCodigoPP = new javax.swing.JTextField();
        txtDniPP = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        txtTelefonoPP = new javax.swing.JTextField();
        btnBuscarPP = new javax.swing.JButton();
        jLabel27 = new javax.swing.JLabel();
        txtnacionalidadPP = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jTabbedPaneRegistro.setFont(new java.awt.Font("Dialog", 3, 12)); // NOI18N
        jTabbedPaneRegistro.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTabbedPaneRegistroMouseClicked(evt);
            }
        });

        jInternalFrame2.setBorder(new javax.swing.border.MatteBorder(null));
        jInternalFrame2.setToolTipText("");
        jInternalFrame2.setVisible(true);

        jLabel1.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel1.setText("Apellidos ");

        jLabel3.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel3.setText("Nombre");

        jLabel4.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel4.setText("Sexo");

        jLabel5.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel5.setText("Fecha Nac");

        buttonGroup1.add(rboMasculino);
        rboMasculino.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        rboMasculino.setText("M");

        buttonGroup1.add(rboFemenino);
        rboFemenino.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        rboFemenino.setText("F");

        jLabel6.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel6.setText("Edad");

        jLabel8.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel8.setText("Dni");

        jLabel9.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel9.setText("Direccion ");

        jLabel10.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel10.setText("Ocupacion");

        jLabel11.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel11.setText("Celular");

        btnguardarPaciente.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnguardarPaciente.setText("Guardar");
        btnguardarPaciente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnguardarPacienteActionPerformed(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel12.setText("Nacionalidad");

        jLabel13.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel13.setText("Codigo");

        txtCodigoPaciente.setEditable(false);

        jLabel14.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel14.setText("Estado Civil");

        calendario.setDateFormatString("yyyy-MM-dd");

        jLabel23.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel23.setText("Tipo de Sangre");

        jLabel25.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel25.setText("Talla ");

        jLabel28.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel28.setText("Peso ");

        jLabel29.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel29.setText("Alergias ");

        javax.swing.GroupLayout jInternalFrame2Layout = new javax.swing.GroupLayout(jInternalFrame2.getContentPane());
        jInternalFrame2.getContentPane().setLayout(jInternalFrame2Layout);
        jInternalFrame2Layout.setHorizontalGroup(
                jInternalFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jInternalFrame2Layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addGroup(jInternalFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jInternalFrame2Layout.createSequentialGroup()
                                        .addGroup(jInternalFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jLabel1)
                                                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jInternalFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(txtDni, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGroup(jInternalFrame2Layout.createSequentialGroup()
                                                        .addGroup(jInternalFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                .addComponent(txtApellido, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGroup(jInternalFrame2Layout.createSequentialGroup()
                                                                        .addComponent(rboMasculino, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                                        .addComponent(rboFemenino, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                .addComponent(txtTipoSangre, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addComponent(txtocupacion, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGap(33, 33, 33)
                                                        .addGroup(jInternalFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                .addComponent(jLabel6)
                                                                .addComponent(jLabel3)
                                                                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGroup(jInternalFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                                        .addComponent(jLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(jLabel11))))
                                                .addComponent(txtAlergias, javax.swing.GroupLayout.PREFERRED_SIZE, 319, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jInternalFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(spEdad, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtDireccion, javax.swing.GroupLayout.DEFAULT_SIZE, 137, Short.MAX_VALUE)
                                .addComponent(txtTalla)
                                .addComponent(txtcelular)
                                .addComponent(txtnombre))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 110, Short.MAX_VALUE)
                        .addGroup(jInternalFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jInternalFrame2Layout.createSequentialGroup()
                                        .addGroup(jInternalFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jLabel12)
                                                .addComponent(jLabel13)
                                                .addGroup(jInternalFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                        .addComponent(jLabel28, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(jLabel14, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                        .addGap(23, 23, 23)
                                        .addGroup(jInternalFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addComponent(calendario, javax.swing.GroupLayout.DEFAULT_SIZE, 123, Short.MAX_VALUE)
                                                .addComponent(txtPeso)
                                                .addComponent(txtEstadoCivil)
                                                .addComponent(txtCodigoPaciente)
                                                .addComponent(txtNacionalidad)))
                                .addComponent(btnguardarPaciente, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(114, Short.MAX_VALUE))
        );
        jInternalFrame2Layout.setVerticalGroup(
                jInternalFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jInternalFrame2Layout.createSequentialGroup()
                        .addGroup(jInternalFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jInternalFrame2Layout.createSequentialGroup()
                                        .addGap(32, 32, 32)
                                        .addGroup(jInternalFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jLabel1)
                                                .addComponent(txtApellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jLabel3)
                                                .addComponent(txtnombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jLabel12)))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jInternalFrame2Layout.createSequentialGroup()
                                        .addContainerGap()
                                        .addComponent(txtNacionalidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(jInternalFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jInternalFrame2Layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jInternalFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jLabel6)
                                                .addComponent(spEdad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(3, 3, 3))
                                .addGroup(jInternalFrame2Layout.createSequentialGroup()
                                        .addGroup(jInternalFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addGroup(jInternalFrame2Layout.createSequentialGroup()
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                        .addComponent(calendario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGroup(jInternalFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(jInternalFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                .addGroup(jInternalFrame2Layout.createSequentialGroup()
                                                                        .addGap(29, 29, 29)
                                                                        .addComponent(jLabel4))
                                                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jInternalFrame2Layout.createSequentialGroup()
                                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addGroup(jInternalFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                                                .addComponent(rboMasculino)
                                                                                .addComponent(rboFemenino))))
                                                        .addGroup(jInternalFrame2Layout.createSequentialGroup()
                                                                .addGap(43, 43, 43)
                                                                .addComponent(jLabel5))))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                        .addGroup(jInternalFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtDni, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel8))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jInternalFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtCodigoPaciente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(27, 27, 27)
                        .addGroup(jInternalFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel10)
                                .addComponent(txtocupacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel11)
                                .addComponent(txtcelular, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel14)
                                .addComponent(txtEstadoCivil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(38, 38, 38)
                        .addGroup(jInternalFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel28)
                                .addComponent(jLabel25)
                                .addComponent(txtTalla, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel23)
                                .addComponent(txtTipoSangre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtPeso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jInternalFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jInternalFrame2Layout.createSequentialGroup()
                                        .addGap(37, 37, 37)
                                        .addGroup(jInternalFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jLabel29)
                                                .addComponent(txtAlergias, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGroup(jInternalFrame2Layout.createSequentialGroup()
                                        .addGap(28, 28, 28)
                                        .addComponent(btnguardarPaciente, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(20, Short.MAX_VALUE))
        );

        jTabbedPane2.addTab("", jInternalFrame2);

        jTabbedPaneRegistro.addTab("Registrar Pacientes", jTabbedPane2);

        jInternalFrame3.setVisible(true);

        jLabeldatosPaciente.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        jLabeldatosPaciente.setText("datos Paciente");

        jLabel2.setText("Descripcion de la cita");

        jLabel19.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel19.setText("Especialidad");

        cboEspecialidades.setModel(new javax.swing.DefaultComboBoxModel<>(new String[]{"seleccionar especialidad"}));
        cboEspecialidades.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboEspecialidadesActionPerformed(evt);
            }
        });

        jLabel18.setText("Profesional ");

        cboProfesional.setModel(new javax.swing.DefaultComboBoxModel<>(new String[]{"seleccionar Dr", ""}));
        cboProfesional.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboProfesionalActionPerformed(evt);
            }
        });

        btnBorrarCampos.setText("Borrar campos");
        btnBorrarCampos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBorrarCamposActionPerformed(evt);
            }
        });

        btnGuardarCita.setText("Guardar");
        btnGuardarCita.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnGuardarCita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarCitaActionPerformed(evt);
            }
        });

        calendarioCita.setDateFormatString("yyyy-MM-dd");

        jLabel7.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        jLabel7.setText("Fecha");

        jLabel16.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel16.setText("Sala ");

        jLabelCodigo.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        jLabelCodigo.setText("Codigo");

        labelResultadoCalendario.setText("............");

        jLabel22.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel22.setText("Hora de la cita");

        labelHora.setText("-----------");

        btnDisponibiidad.setText("ver disponibilidad");
        btnDisponibiidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDisponibiidadActionPerformed(evt);
            }
        });

        cboHorasTotales.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboHorasTotalesActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jInternalFrame3Layout = new javax.swing.GroupLayout(jInternalFrame3.getContentPane());
        jInternalFrame3.getContentPane().setLayout(jInternalFrame3Layout);
        jInternalFrame3Layout.setHorizontalGroup(
                jInternalFrame3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jInternalFrame3Layout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addGroup(jInternalFrame3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel19)
                                .addGroup(jInternalFrame3Layout.createSequentialGroup()
                                        .addGroup(jInternalFrame3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(jInternalFrame3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                        .addComponent(txtDescripcionCita)
                                                        .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(cboProfesional, 0, 163, Short.MAX_VALUE)
                                                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addComponent(cboEspecialidades, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jLabeldatosPaciente, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(btnBorrarCampos, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(jInternalFrame3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(jInternalFrame3Layout.createSequentialGroup()
                                                        .addGap(159, 159, 159)
                                                        .addComponent(jLabelCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGroup(jInternalFrame3Layout.createSequentialGroup()
                                                        .addGap(147, 147, 147)
                                                        .addGroup(jInternalFrame3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addComponent(txtSala, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGroup(jInternalFrame3Layout.createSequentialGroup()
                                                                        .addGroup(jInternalFrame3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                                                .addComponent(calendarioCita, javax.swing.GroupLayout.DEFAULT_SIZE, 129, Short.MAX_VALUE)
                                                                                .addComponent(cboHorasTotales, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                                                        .addGap(46, 46, 46)
                                                                        .addGroup(jInternalFrame3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                                                .addComponent(labelResultadoCalendario, javax.swing.GroupLayout.DEFAULT_SIZE, 129, Short.MAX_VALUE)
                                                                                .addComponent(labelHora, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                                                .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGroup(jInternalFrame3Layout.createSequentialGroup()
                                                                        .addComponent(btnGuardarCita, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addGap(93, 93, 93)
                                                                        .addComponent(btnDisponibiidad)))))))
                        .addContainerGap(278, Short.MAX_VALUE))
        );
        jInternalFrame3Layout.setVerticalGroup(
                jInternalFrame3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jInternalFrame3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jInternalFrame3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabeldatosPaciente)
                                .addComponent(jLabelCodigo))
                        .addGap(9, 9, 9)
                        .addGroup(jInternalFrame3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel16)
                                .addComponent(jLabel2))
                        .addGap(18, 18, 18)
                        .addGroup(jInternalFrame3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtDescripcionCita, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtSala, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jInternalFrame3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel19)
                                .addComponent(jLabel7))
                        .addGroup(jInternalFrame3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jInternalFrame3Layout.createSequentialGroup()
                                        .addGap(21, 21, 21)
                                        .addGroup(jInternalFrame3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addComponent(cboEspecialidades, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(calendarioCita, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jInternalFrame3Layout.createSequentialGroup()
                                        .addGap(26, 26, 26)
                                        .addComponent(labelResultadoCalendario, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(41, 41, 41)
                        .addGroup(jInternalFrame3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel18)
                                .addComponent(jLabel22))
                        .addGroup(jInternalFrame3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jInternalFrame3Layout.createSequentialGroup()
                                        .addGap(24, 24, 24)
                                        .addGroup(jInternalFrame3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addComponent(cboProfesional, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(labelHora)))
                                .addGroup(jInternalFrame3Layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addComponent(cboHorasTotales, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(34, 34, 34)
                        .addGroup(jInternalFrame3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btnBorrarCampos, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnGuardarCita)
                                .addComponent(btnDisponibiidad))
                        .addContainerGap(42, Short.MAX_VALUE))
        );

        jTabbedPaneRegistro.addTab("Consulta", jInternalFrame3);

        jInternalFrame1.setVisible(true);

        jLabel15.setText("Codigo ");

        jLabel17.setText("DNI");

        jLabel20.setText("NOMBRES");

        jLabel21.setText("APELLIDO PATERNO");

        jLabel24.setText("EDAD");

        jLabel26.setText("TELEFONO");

        btnBuscarPP.setText("BUSCAR ");
        btnBuscarPP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarPPActionPerformed(evt);
            }
        });

        jLabel27.setText("NACIONALIDAD");

        javax.swing.GroupLayout jInternalFrame1Layout = new javax.swing.GroupLayout(jInternalFrame1.getContentPane());
        jInternalFrame1.getContentPane().setLayout(jInternalFrame1Layout);
        jInternalFrame1Layout.setHorizontalGroup(
                jInternalFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jInternalFrame1Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnBuscarPP, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(42, 42, 42))
                .addGroup(jInternalFrame1Layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addGroup(jInternalFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jInternalFrame1Layout.createSequentialGroup()
                                        .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(81, 81, 81)
                                        .addComponent(txtNombresPP, javax.swing.GroupLayout.PREFERRED_SIZE, 233, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jInternalFrame1Layout.createSequentialGroup()
                                        .addGroup(jInternalFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(jInternalFrame1Layout.createSequentialGroup()
                                                        .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                        .addComponent(txtCodigoPP, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addGap(126, 126, 126)
                                                        .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGroup(jInternalFrame1Layout.createSequentialGroup()
                                                        .addGroup(jInternalFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGap(81, 81, 81)
                                                        .addGroup(jInternalFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                .addComponent(txtApellidosPP, javax.swing.GroupLayout.PREFERRED_SIZE, 233, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addComponent(txtEdadPP, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addComponent(txtTelefonoPP, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addComponent(txtnacionalidadPP, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtDniPP, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(381, Short.MAX_VALUE))
        );
        jInternalFrame1Layout.setVerticalGroup(
                jInternalFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jInternalFrame1Layout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addGroup(jInternalFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtCodigoPP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtDniPP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(8, 8, 8)
                        .addComponent(btnBuscarPP)
                        .addGap(11, 11, 11)
                        .addGroup(jInternalFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtNombresPP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(39, 39, 39)
                        .addGroup(jInternalFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtApellidosPP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jInternalFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtEdadPP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(27, 27, 27)
                        .addGroup(jInternalFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtTelefonoPP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(22, 22, 22)
                        .addGroup(jInternalFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtnacionalidadPP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(38, Short.MAX_VALUE))
        );

        jTabbedPaneRegistro.addTab("Perfil Paciente ", jInternalFrame1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jTabbedPaneRegistro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jTabbedPaneRegistro)
                        .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTabbedPaneRegistroMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTabbedPaneRegistroMouseClicked
        if (txtCodigoPaciente != null && txtnombre != null) {
            jLabelCodigo.setText(txtCodigoPaciente.getText());
            jLabeldatosPaciente.setText(txtnombre.getText() + " " + txtApellido.getText());
        }
        if (txtCodigoPP.getText() != null && txtNombresPP.getText() != null) {
            jLabelCodigo.setText(txtCodigoPP.getText());
            jLabeldatosPaciente.setText(txtNombresPP.getText() + " " + txtApellidosPP.getText());
        }
    }//GEN-LAST:event_jTabbedPaneRegistroMouseClicked

    private void btnGuardarCitaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarCitaActionPerformed

        rbd = new registrarBasedatos();
        cita = new Cita();
        cita.crearCodigo();
        cita.crearAcceso();
        cita.setMedico(med);
        cita.setDescripcion(txtDescripcionCita.getText());
        cita.setFechaCita(new java.sql.Date(calendarioCita.getDate().getTime()));
        if (!(p.getCodigop().equals(""))) {
            cita.getPaciente().setCodigop(p.getCodigop());
        } else {
            cita.getPaciente().setCodigop(jLabelCodigo.getText());
        }
        cita.setHoraCita(Time.valueOf(hora));
        if (rbd.RegistrarCita(cita) && rbd.RegistrarTurno(med.getCodigoM(), codigoHora, cita.getFechaCita())) {
            JOptionPane.showMessageDialog(null, "Cita registrada con exito", "", JOptionPane.INFORMATION_MESSAGE);
            limpiarCita();
        } else {
            JOptionPane.showMessageDialog(null, " Error al registrar", "", JOptionPane.ERROR);
        }
    }//GEN-LAST:event_btnGuardarCitaActionPerformed

    private void limpiarCita() {
        txtSala.setText("");
        txtDescripcionCita.setText("");
        cboEspecialidades.setSelectedItem(null);
        cboHorasTotales.setSelectedItem(null);
        cboProfesional.setSelectedItem(null);
        calendarioCita.setDate(null);
        labelHora.setVisible(false);
        labelResultadoCalendario.setVisible(false);
    }

    private void btnBorrarCamposActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBorrarCamposActionPerformed

        txtSala.setText("");
        txtDescripcionCita.setText("");
        cboEspecialidades.setSelectedItem(null);
        cboHorasTotales.setSelectedItem(null);
        cboProfesional.setSelectedItem(null);
        calendarioCita.setDate(null);
    }//GEN-LAST:event_btnBorrarCamposActionPerformed

    private void cboEspecialidadesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboEspecialidadesActionPerformed
        int opcion = cboEspecialidades.getSelectedIndex();
        lista = new ArrayList<>();
        ListaMedico lis = new ListaMedico();
        switch (opcion) {
            case 1:
                lista = lis.buscarMedicosPorEspecialidad((String) cboEspecialidades.getSelectedItem());
                mostrarCboProfecional(lista);
                break;
            case 2:
                lista = lis.buscarMedicosPorEspecialidad((String) cboEspecialidades.getSelectedItem());
                mostrarCboProfecional(lista);
                break;
            case 3:
                lista = lis.buscarMedicosPorEspecialidad((String) cboEspecialidades.getSelectedItem());
                mostrarCboProfecional(lista);
                break;
            case 4:
                lis.buscarMedicosPorEspecialidad((String) cboEspecialidades.getSelectedItem());
                mostrarCboProfecional(lista);
                break;
            case 5:
                lista = lis.buscarMedicosPorEspecialidad((String) cboEspecialidades.getSelectedItem());
                mostrarCboProfecional(lista);
                break;

        }
    }//GEN-LAST:event_cboEspecialidadesActionPerformed

    private void btnguardarPacienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnguardarPacienteActionPerformed
        String sexo = new String();

        rbd = new registrarBasedatos();
        p = new Persona();
        p.crearCodigoPac();
        if (rboFemenino.isSelected()) {
            sexo = "F";
        }
        if (rboMasculino.isSelected()) {
            sexo = "M";
        }
        p.setApellido(txtApellido.getText());
        p.setEdad((int) spEdad.getValue());
        p.setDireccion(txtDireccion.getText());
        p.setDni(txtDni.getText());
        p.setTelefono(txtcelular.getText());
        p.setNombre(txtnombre.getText());
        p.setOcupacion(txtocupacion.getText());
        p.setSexo(sexo);
        p.setEstadoCivil(txtEstadoCivil.getText());
        p.setNacionalidad(txtNacionalidad.getText());
        p.setFechaNac(new java.sql.Date(calendario.getDate().getTime()));
        if (rbd.registrarPaciente(p, txtTipoSangre.getText(), txtTalla.getText(), txtPeso.getText(), txtAlergias.getText())) {
            JOptionPane.showMessageDialog(null, "Datos registrados con exito", "", JOptionPane.INFORMATION_MESSAGE);
            limpiar();
        } else {
            JOptionPane.showMessageDialog(null, " Error al registrar", "", JOptionPane.ERROR);
            limpiar();
        }
    }//GEN-LAST:event_btnguardarPacienteActionPerformed

    private void limpiar() {
        txtAlergias.setText("");
        txtApellido.setText("");
        txtCodigoPaciente.setText("");
        txtDireccion.setText("");
        txtDni.setText("");
        txtEstadoCivil.setText("");
        txtNacionalidad.setText("");
        txtTalla.setText("");
        txtPeso.setText("");
        txtTipoSangre.setText("");
        txtocupacion.setText("");
        txtnombre.setText("");
        txtcelular.setText("");
        spEdad.setValue(0);
        calendario.setDate(null);
        buttonGroup1.clearSelection();

    }

    private void btnBuscarPPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarPPActionPerformed
        cita = new Cita();
        if (txtDniPP.getText() != null) {
            Persona pe = cita.BuscarPaciente(txtDniPP.getText());
            if (pe != null) {
                txtCodigoPP.setText(pe.getCodigop());
                txtNombresPP.setText(pe.getNombre());
                txtApellidosPP.setText(pe.getApellido());
                txtEdadPP.setText(String.valueOf(pe.getEdad()));
                txtTelefonoPP.setText(pe.getTelefono());
                txtnacionalidadPP.setText(pe.getNacionalidad());
            } else {
                JOptionPane.showMessageDialog(null, "paciente no encontrado ");
                txtDniPP.setText("");
                txtDni.requestFocus();
            }
        }
    }//GEN-LAST:event_btnBuscarPPActionPerformed

    private void cboProfesionalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboProfesionalActionPerformed
        int opcion = cboProfesional.getSelectedIndex();
        switch (opcion) {
            case 0:
                med = buscarMedico((String) cboProfesional.getSelectedItem(), lista);
                break;
            case 2:
                med = buscarMedico((String) cboProfesional.getSelectedItem(), lista);
                break;
            case 3:
                med = buscarMedico((String) cboProfesional.getSelectedItem(), lista);
                break;
            case 4:
                med = buscarMedico((String) cboProfesional.getSelectedItem(), lista);
                break;
            case 5:
                med = buscarMedico((String) cboProfesional.getSelectedItem(), lista);
                break;
            case 6:
                med = buscarMedico((String) cboProfesional.getSelectedItem(), lista);
                break;
            case 7:
                med = buscarMedico((String) cboProfesional.getSelectedItem(), lista);
                break;
            case 8:
                med = buscarMedico((String) cboProfesional.getSelectedItem(), lista);
                break;
        }
        mostrarCboHoras();

    }//GEN-LAST:event_cboProfesionalActionPerformed

    private void mostrarCboHoras() {
        Horario h = new Horario();
        int horas = h.getHorasTotalesDia(med.getHoraFin(), med.getHoraInicio());
        String[] ho = h.getHoras(med.getHoraFin(), med.getHoraInicio(), horas);
        for (int i = 0; i < ho.length; i++) {
            cboHorasTotales.addItem(ho[i]);
        }
        cboHorasTotales.setVisible(true);
    }

    private void btnDisponibiidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDisponibiidadActionPerformed

        rbd = new registrarBasedatos();
        Horario h = new Horario();
        int horasTotales = h.getHorasTotalesDia(med.getHoraFin(), med.getHoraInicio());
        codigoHora = h.obtenerCodigo(hora);
        if (rbd.verificarFecha(med.getCodigoM(), new java.sql.Date(calendarioCita.getDate().getTime()), horasTotales)) {
            labelResultadoCalendario.setText("No disponible");
            labelResultadoCalendario.setBackground(Color.red);
            labelResultadoCalendario.setVisible(true);
            btnGuardarCita.setEnabled(false);

        } else {
            labelResultadoCalendario.setText("Disponible");
            labelResultadoCalendario.setBackground(Color.green);
            labelResultadoCalendario.setVisible(true);
            btnGuardarCita.setEnabled(true);
            btnGuardarCita.setVisible(true);
        }
        if (rbd.verificarHorario(med.getCodigoM(), new java.sql.Date(calendarioCita.getDate().getTime()), codigoHora)) {
            labelHora.setText("no disponible");
            labelHora.setBackground(Color.red);
            labelHora.setVisible(true);
            btnGuardarCita.setEnabled(false);
        } else {
            labelHora.setText("disponible");
            labelHora.setBackground(Color.green);
            labelHora.setVisible(true);
            btnGuardarCita.setEnabled(true);
            btnGuardarCita.setVisible(true);
        }

    }//GEN-LAST:event_btnDisponibiidadActionPerformed

    private void cboHorasTotalesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboHorasTotalesActionPerformed
        hora = (String) cboHorasTotales.getSelectedItem();
    }//GEN-LAST:event_cboHorasTotalesActionPerformed

    private void cboEspecialidades() {
        ListaMedico lis = new ListaMedico();
        ArrayList<String> lista = lis.buscarEspecialidades();
        cboProfesional.setVisible(false);
        lista.stream().forEach((s) -> {
            cboEspecialidades.addItem(s);
        });
    }

    private void mostrarCboProfecional(ArrayList<Medico> l) {
        l.stream().forEach((m) -> {
            cboProfesional.addItem(m.getApellido() + "" + m.getNombre());
        });
        cboProfesional.setVisible(true);
    }

    private Medico buscarMedico(String apellido, ArrayList<Medico> l) {
        Medico m = new Medico();
        for (Medico me : l) {
            if ((me.getApellido() + "" + me.getNombre()).equals(apellido)) {
                m = me;
            }
        }
        return m;
    }
    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnBorrarCampos;
    private javax.swing.JButton btnBuscarPP;
    private javax.swing.JButton btnDisponibiidad;
    public javax.swing.JButton btnGuardarCita;
    public javax.swing.JButton btnguardarPaciente;
    private javax.swing.ButtonGroup buttonGroup1;
    private com.toedter.calendar.JDateChooser calendario;
    private com.toedter.calendar.JDateChooser calendarioCita;
    private javax.swing.JComboBox<String> cboEspecialidades;
    private javax.swing.JComboBox<String> cboHorasTotales;
    public javax.swing.JComboBox<String> cboProfesional;
    private javax.swing.JInternalFrame jInternalFrame1;
    private javax.swing.JInternalFrame jInternalFrame2;
    public javax.swing.JInternalFrame jInternalFrame3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    public javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    public javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    public javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    public javax.swing.JLabel jLabelCodigo;
    public javax.swing.JLabel jLabeldatosPaciente;
    private javax.swing.JTabbedPane jTabbedPane2;
    public javax.swing.JTabbedPane jTabbedPaneRegistro;
    private javax.swing.JLabel labelHora;
    private javax.swing.JLabel labelResultadoCalendario;
    public javax.swing.JRadioButton rboFemenino;
    public javax.swing.JRadioButton rboMasculino;
    public javax.swing.JSpinner spEdad;
    private javax.swing.JTextField txtAlergias;
    public javax.swing.JTextField txtApellido;
    private javax.swing.JTextField txtApellidosPP;
    private javax.swing.JTextField txtCodigoPP;
    public javax.swing.JTextField txtCodigoPaciente;
    public javax.swing.JTextField txtDescripcionCita;
    public javax.swing.JTextField txtDireccion;
    public javax.swing.JTextField txtDni;
    public javax.swing.JTextField txtDniPP;
    private javax.swing.JTextField txtEdadPP;
    public javax.swing.JTextField txtEstadoCivil;
    public javax.swing.JTextField txtNacionalidad;
    private javax.swing.JTextField txtNombresPP;
    private javax.swing.JTextField txtPeso;
    public javax.swing.JTextField txtSala;
    private javax.swing.JTextField txtTalla;
    private javax.swing.JTextField txtTelefonoPP;
    private javax.swing.JTextField txtTipoSangre;
    public javax.swing.JTextField txtcelular;
    private javax.swing.JTextField txtnacionalidadPP;
    public javax.swing.JTextField txtnombre;
    public javax.swing.JTextField txtocupacion;
    // End of variables declaration//GEN-END:variables

}
