package listas;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import complementos.Conexion;
import informacion.Cita;
import informacion.Historia;
import informacion.Medico;
import informacion.Persona;
import nodos.NodoHistorial;
public class Historiales{
	private String codigoHistorial;
	private double talla;
	private double peso;
	private String tipoSangre;
	private String alergias;
	private Persona paciente;
	private String codigoPaciente;
	private NodoHistorial raiz;
	private int contador;
	public Historiales(String codigoPaciente){
		raiz=null;
		contador=0;
		this.setCodigoPaciente(codigoPaciente);
		pushDataBase();
	}
	public Historia getHistoria(int i){
		NodoHistorial aux=raiz;
			while(i!=0){
			aux=aux.getNext();
			i--;
			}
		return aux.getHistoria();
	}
	public void pushDataBase(){
		Persona paciente=null;
		Historia historia=null;
		Medico medico=null;
		Cita cita=null;
		NodoHistorial nodo;
		try {
			Connection cn=Conexion.getConexionMYSQL();
			Statement st=cn.createStatement();
			ResultSet rs=st.executeQuery("select * from historiales inner join persona on historiales.codigoPaciente=persona.codigo where codigoPaciente='"+this.codigoPaciente+"';");
			while(rs.next()){
				paciente=new Persona(rs.getString("nombres"),rs.getString("apellidos"),rs.getInt("edad"),rs.getString("sexo"),rs.getDate("fechaNac")
						,rs.getString("telefono"),rs.getString("nacionalidad"),rs.getString("direccion"),rs.getString("estadoCivil"),rs.getString("ocupacion"),
						rs.getString("dni"),rs.getString("codigo"));
			}
			this.paciente=paciente;
			rs.close();
			Statement st1= cn.createStatement();
			ResultSet rs1=st1.executeQuery("select * from historiales inner join historia on historiales.codigoHistorial=historia.codigoHistorial"
					+ " inner join citas on historia.codigoCita=citas.codigoCita inner join medicos on citas.codigoMedico=medicos.codigoMedico inner join"
					+ " persona on medicos.codigoMedico=persona.codigo where historiales.codigoPaciente='"+this.codigoPaciente+"';");
			while(rs1.next()){
				this.setCodigoHistoria(rs1.getString("codigoHistorial"));
				this.setPeso(rs1.getDouble("peso"));
				this.setTalla(rs1.getDouble("talla"));
				this.setAlergias(rs1.getString("Alergias"));
				this.setTipoSangre(rs1.getString("tipoSangre"));
				medico=new Medico(rs1.getString("nombres"),rs1.getString("apellidos"),rs1.getInt("edad"),rs1.getString("sexo"),rs1.getString("telefono")
				,rs1.getString("nacionalidad"),rs1.getString("direccion"),rs1.getString("estadoCivil"),rs1.getString("dni"),rs1.getString("codigoMedico"),
				rs1.getInt("numeroConsultorio"),rs1.getString("especialidad"),rs1.getString("dias"),rs1.getTime("horaInicio").toString(),rs1.getTime("horaFin").toString());
				cita= new Cita(rs1.getString("codigoCita"),medico,rs1.getDate("fechaCita"),rs1.getTime("horaCita"),rs1.getString("descripcion"));
				historia=new Historia(rs1.getString("diagnostico"),rs1.getString("receta"),cita,rs1.getString("descripcion"));
				nodo=new NodoHistorial(historia);
				this.push(nodo);
			}
			rs1.close();
			st1.close();
			cn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public void push(NodoHistorial nodo){
		if(raiz==null){
			setRaiz(nodo);
			contador++;
		}
		else{
			NodoHistorial aux=raiz;
			while(aux.getNext()!=null){
				aux=aux.getNext();
			}
			aux.setNext(nodo);
			contador++;
		}
	}
	
	public String getCodigoHistorial() {
		return codigoHistorial;
	}
	public void setCodigoHistorial(String codigoHistorial) {
		this.codigoHistorial = codigoHistorial;
	}
	public double getTalla() {
		return talla;
	}
	public void setTalla(double talla) {
		this.talla = talla;
	}
	public double getPeso() {
		return peso;
	}
	public void setPeso(double peso) {
		this.peso = peso;
	}
	public String getTipoSangre() {
		return tipoSangre;
	}
	public void setTipoSangre(String tipoSangre) {
		this.tipoSangre = tipoSangre;
	}
	public String getCodigoHistoria() {
		return codigoHistorial;
	}
	public void setCodigoHistoria(String codigoHistoria) {
		this.codigoHistorial = codigoHistoria;
	}
	public NodoHistorial getRaiz() {
		return raiz;
	}
	public void setRaiz(NodoHistorial raiz) {
		this.raiz = raiz;
	}
	public int getContador() {
		return contador;
	}
	public void setContador(int contador) {
		this.contador = contador;
	}
	public String getCodigoPaciente() {
		return codigoPaciente;
	}
	public void setCodigoPaciente(String codigoPaciente) {
		this.codigoPaciente = codigoPaciente;
	}
	public Persona getPaciente() {
		return paciente;
	}
	public void setPaciente(Persona paciente) {
		this.paciente = paciente;
	}
	public String getAlergias() {
		return alergias;
	}
	public void setAlergias(String alergias) {
		this.alergias = alergias;
	}

}
