package nodos;

import listas.Historiales;

public class NodoHistoriales {
	private Historiales historial;
	private NodoHistoriales next;
	public NodoHistoriales(Historiales h){
		historial=h;
		next=null;
	}
	public Historiales getHistorial() {
		return historial;
	}
	public void setHistorial(Historiales historial) {
		this.historial = historial;
	}
	public NodoHistoriales getNext() {
		return next;
	}
	public void setNext(NodoHistoriales next) {
		this.next = next;
	}
}
