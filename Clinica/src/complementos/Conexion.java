package complementos;

import java.sql.Connection;
import java.sql.DriverManager;

import javax.swing.JOptionPane;

public class Conexion {

    private static String usuario = "root";

    private final static String pass = "Bamby.124"; 

	private static String connectionString = "jdbc:mysql://localhost:3306/clinica?useSSL=false&zeroDateTimeBehavior=convertToNull&characterEncoding=UTF-8&characterSetResults=UTF-8&autoReconnect=true";

    private static Connection con = null;
    
    public static Connection getConexionMYSQL(){
        try{
            Class.forName("com.mysql.jdbc.Driver").newInstance( );
            con = DriverManager.getConnection(connectionString,usuario,pass);
            return con;
        }catch(Exception e){
            e.printStackTrace();
            JOptionPane.showMessageDialog(null,"Fallo en la conexion a la Base de Datos");
            return con;
        }
    }
}