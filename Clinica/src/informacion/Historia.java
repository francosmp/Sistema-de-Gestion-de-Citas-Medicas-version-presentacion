package informacion;

import complementos.Conexion;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Historia {
	private String diagnostico;
	private String receta;
	private Cita cita;
	private String  descripcion;
	public Historia(String diagnostico, String receta, Cita cita,String descripcion) {
		super();
		this.diagnostico = diagnostico;
		this.receta = receta;
		this.cita = cita;
		this.descripcion=descripcion;
	}

    public Historia() {
        
    }
	public String getDiagnostico() {
		return diagnostico;
	}
	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}
	public String getReceta() {
		return receta;
	}
	public void setReceta(String receta) {
		this.receta = receta;
	}
	public Cita getCita() {
		return cita;
	}
	public void setCita(Cita cita) {
		this.cita = cita;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
         public String crearCodigo(){
            Connection cn = Conexion.getConexionMYSQL();
            String codigo = null ;
        int contador = 0;
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery("SELECT codigoHistorial FROM historia");
            while (rs.next()) {
                contador++;
            }
          codigo = "HI" + String.valueOf(contador + 1);
          
        } catch (SQLException e) {
            System.out.println(e.getMessage()+"crear codigo");
        }
        return codigo;
        }
}