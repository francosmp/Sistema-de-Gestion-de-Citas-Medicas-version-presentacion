package paneles;

import java.awt.BorderLayout;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import complementos.Conexion;

public class TablaAtencion extends JPanel implements Runnable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String area;
	private JTable tabla;
	private JScrollPane scroll;
	public Vector<String[]> cache;
	public Vector<String> estado;
	public Vector<String> auxiliar;
	String[] headers={"Nombre del Paciente","Estado de la cita"};
	public TablaAtencion(String area){
		super();
		this.area=area;
		this.setLayout(new BorderLayout());
		pushDatos();
		crearTabla();
	}
	public void pushDatos(){
		cache=null;
		estado=null;
		cache=new Vector<String[]>();
		estado=new Vector<String>();
		Connection cn=null;
		try{
			Date d=new Date();
			@SuppressWarnings("deprecation")
			String dia= String.valueOf(1900+d.getYear())+"-"+String.valueOf(d.getMonth()+1)+"-"+d.getDate();
			cn=Conexion.getConexionMYSQL();
			Statement st= cn.createStatement();
			ResultSet rs=st.executeQuery("SELECT * FROM citas inner join medicos on citas.codigoMedico=medicos.codigoMedico "
					+ " inner join persona on persona.codigo=citas.codigoPaciente where especialidad='"+area+"' and citas.fechaCita='"+dia+"';");
			while(rs.next()){
				String[] record= new String[2];
				record[0]=rs.getString("nombres")+rs.getString("apellidos");
				record[1]=rs.getString("descripcion");
				estado.addElement(record[1]);
				cache.addElement(record);
			}
			cn.close();
			st.close();
			rs.close();
		}catch(Exception e){
			e.printStackTrace();
			JOptionPane.showMessageDialog(null,"Fallo en el llenado de tabla atencion");
		}
	}
	public void crearTabla(){
		// TODO Auto-generated method stub
			tabla=new JTable();
			Object[][] o= cache.toArray(new Object[cache.size()][2]);
			getJTable(o,headers);
			scroll = new JScrollPane(tabla);
			this.add(scroll,BorderLayout.CENTER);
	}
	public void getJTable(Object[][] data,String[] columnNames){
		DefaultTableModel model = new DefaultTableModel() {

		    /**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
		    public boolean isCellEditable(int row, int column) {
	        		return false;
		       //all cells false
		    }
		};
		TableRowSorter<DefaultTableModel> sorter = new TableRowSorter<DefaultTableModel>(model);
		model.setDataVector(data, columnNames);
		tabla.setModel(model);
		tabla.setRowSorter(sorter);
		tabla.setRowSelectionAllowed(false);
		tabla.getTableHeader().setReorderingAllowed(false);
}
	public void verificarEstado() {
		// TODO Auto-generated method stub
		auxiliar=null;
		auxiliar=new Vector<String>();
		for(int i=0; i<estado.size();i++){
			String v=new String(estado.elementAt(i));
			auxiliar.addElement(v);
		}
		pushDatos();
		boolean flag=true;
		for(int j=0; j<auxiliar.size() && flag;j++){
			String a=new String(auxiliar.elementAt(j));
			String b=new String(estado.elementAt(j));
			if(!a.equals(b)){
				flag=false;
			}
		}
		if(!flag){
			this.removeAll();
			crearTabla();
			this.updateUI();
		}
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		while(true)
		verificarEstado();
	}
}
