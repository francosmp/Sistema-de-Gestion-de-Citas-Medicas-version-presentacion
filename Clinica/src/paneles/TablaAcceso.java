package paneles;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableRowSorter;

import complementos.Conexion;
import complementos.MyTableModel;
public class TablaAcceso extends JPanel implements ActionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JTable tabla;
	JScrollPane scroll;
	ArrayList<JButton> botones;
	private MyTableModel modelo;
	ArrayList<String> codigoCita;
	public TablaAcceso(String codigo){
		tabla= new JTable();
		codigoCita=new ArrayList<String>();
		botones=new ArrayList<JButton>();
		scroll=new JScrollPane();
		ArrayList<Object[]> data = new ArrayList<Object[]>();
		int i=0;
		tabla= new JTable(){
	        /**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			public boolean isCellEditable(int rowIndex, int vColIndex) {
	        	if(vColIndex!=4)
	        		return false;
	        	else
	        		return true;
	        }};
		try{
			Connection cn=Conexion.getConexionMYSQL();
			Statement st= cn.createStatement();
			ResultSet rs=st.executeQuery("select * from citas inner join persona"
					+ " on citas.codigoPaciente=persona.codigo where codigoMedico='"+codigo+"';");	
			if(rs.next()){
				rs.beforeFirst();
				while(rs.next()){
					botones.add(new JButton("Dar Acceso"));
					botones.get(i).addActionListener(this);
					data.add(new Object[]{rs.getString("codigoCita"),rs.getString("nombres")+" "+rs.getString("apellidos")
					,rs.getDate("fechaCita").toString(),rs.getString("acceso"),botones.get(i)});
					codigoCita.add(rs.getString("codigoCita"));
					i++;
				}
				Object[][] o= data.toArray(new Object[data.size()][4]);
				setTable(o);
				scroll.setViewportView(tabla);
				this.add(scroll);
			}
			else{
				JOptionPane.showMessageDialog(null,"�Codigo Incorrecto!");
			}
			cn.close();
			st.close();
			rs.close();

			}catch(Exception e){
				e.printStackTrace();
			}
	}
	private void setTable(Object[][] data){
		modelo = new MyTableModel(new String[]{"Codigo Cita","Paciente","Fecha","Codigo de acceso",""},data);
		TableRowSorter<MyTableModel> sorter = new TableRowSorter<MyTableModel>(modelo);
		tabla.setModel(modelo);
		tabla.setDefaultRenderer(JButton.class, new complementos.ButtonCellRenderer());
		tabla.setDefaultEditor(JButton.class, new complementos.ButtonCellEditor());
		tabla.setRowSorter(sorter);
		tabla.setRowSelectionAllowed(false);
		tabla.getTableHeader().setReorderingAllowed(false);
}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		int cantBotones=botones.size();
		for(int i=0; i<cantBotones; i++){
			if(e.getSource()==botones.get(i)){
				try{
					Connection cn=Conexion.getConexionMYSQL();
					Statement st= cn.createStatement();
					st.executeUpdate("update citas set permiso='1' where codigoCita='"+codigoCita.get(i)+"';");
					
				}catch(Exception E){
					E.printStackTrace();
				}
			}
		}
	}

}
