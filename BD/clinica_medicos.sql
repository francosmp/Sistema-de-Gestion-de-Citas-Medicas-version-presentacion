-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: clinica
-- ------------------------------------------------------
-- Server version	5.7.12-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `medicos`
--

DROP TABLE IF EXISTS `medicos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medicos` (
  `codigoMedico` varchar(20) NOT NULL,
  `numeroConsultorio` tinyint(3) NOT NULL,
  `especialidad` varchar(45) NOT NULL,
  `dias` varchar(5) NOT NULL,
  `horaInicio` time(4) NOT NULL,
  `horaFin` time(4) NOT NULL,
  PRIMARY KEY (`codigoMedico`),
  CONSTRAINT `codmed` FOREIGN KEY (`codigoMedico`) REFERENCES `persona` (`codigo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medicos`
--

LOCK TABLES `medicos` WRITE;
/*!40000 ALTER TABLE `medicos` DISABLE KEYS */;
INSERT INTO `medicos` VALUES ('MED02',12,'urologia','L-V','08:00:00.0000','17:00:00.0000'),('MED03',1,'Gastroenterologia','L-V','08:00:00.0000','17:00:00.0000'),('MED04',3,'Gastroenterologia','L-V','08:00:00.0000','17:00:00.0000'),('MED05',4,'Oftalmologia','L-V','08:00:00.0000','17:00:00.0000'),('MED06',5,'Oftalmologia','L-V','08:00:00.0000','17:00:00.0000'),('MED07',6,'Toxicologia','L-V','08:00:00.0000','17:00:00.0000'),('MED08',7,'Toxicologia','L-V','08:00:00.0000','17:00:00.0000'),('MED09',8,'Psiquiatria','L-V','08:00:00.0000','19:00:00.0000'),('MED10',11,'Psiquiatria','l_v','08:00:00.0000','16:00:00.0000'),('MED11',10,'Cardiologia','L-V','08:00:00.0000','17:00:00.0000'),('MED12',12,'Toxicologia','L-V','08:00:00.0000','16:00:00.0000'),('MED13',9,'Toxicologia','L-V','08:00:00.0000','16:00:00.0000'),('MED14',13,'Oftalmologia','L-V','08:00:00.0000','12:00:00.0000'),('MED15',15,'Cardiologia','L-V','08:00:00.0000','16:00:00.0000'),('MED16',16,'Gastroenterologia','L-V','08:00:00.0000','16:00:00.0000'),('MED17',17,'Gastroenterologia','L-V','08:00:00.0000','17:00:00.0000'),('MED18',18,'Psiquiatria','L-V','08:00:00.0000','18:00:00.0000'),('MED19',19,'Oftalmologia','L-V','08:00:00.0000','16:00:00.0000');
/*!40000 ALTER TABLE `medicos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-23 18:30:25
