package informacion;
public class Horario {

    private String[] codigoHora = {"T1", "T2", "T3", "T4", "T5", "T6", "T7", "T8", "T9", "T10", "T11", "T12", "T13", "T14", "T15", "T16"};
    public String[] hora = {"08:00:00", "09:00:00", "10:00:00", "11:00:00", "12:00:00", "13:00:00", "14:00:00", "15:00:00", "16:00:00", "17:00:00", "18:00:00", "19:00:00", "20:00:00", "21:00:00", "10:00:00", "22:00:00"};
    String codigo = null;

    public String obtenerCodigo(String hor) {
        boolean encontrado = false;
        for (int i = 0; i < hora.length && !encontrado; i++) {
            if (hor.equals(hora[i])) {
                codigo = codigoHora[i];
                encontrado = true;
            }
        }
        return codigo;
    }

    public int getHorasTotalesDia(String horaF, String horaI) {
        char[] hF = horaF.toCharArray();
        char[] hI = horaI.toCharArray();
        String cadenaF = "", cadenaI = "";
        for (int i = 0; i < 2; i++) {
            cadenaF = cadenaF + hF[i];
            cadenaI = cadenaI + hI[i];
        }
        return Integer.parseInt(cadenaF) - Integer.parseInt(cadenaI);
    }

    public String[] getHoras(String horaF, String horaI, int horas) {
        String[] ho = new String[horas];
        int i = 0, j = 0;
        for (int k = 0; k < hora.length; k++) {
            if (hora[k].equals(horaI)) {
                while (j < horas) {
                    ho[i] = hora[k];
                    i++;
                    j++;
                    k++;
                }

            }
        }
        return ho;
    }
}
