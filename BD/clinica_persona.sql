-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: clinica
-- ------------------------------------------------------
-- Server version	5.7.12-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `persona`
--

DROP TABLE IF EXISTS `persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persona` (
  `codigo` varchar(20) NOT NULL,
  `nombres` varchar(60) NOT NULL,
  `apellidos` varchar(60) NOT NULL,
  `edad` tinyint(3) NOT NULL,
  `sexo` varchar(1) NOT NULL,
  `fechaNac` date DEFAULT NULL,
  `telefono` varchar(10) NOT NULL,
  `nacionalidad` varchar(25) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `estadoCivil` varchar(45) NOT NULL,
  `ocupacion` varchar(45) DEFAULT NULL,
  `dni` varchar(8) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persona`
--

LOCK TABLES `persona` WRITE;
/*!40000 ALTER TABLE `persona` DISABLE KEYS */;
INSERT INTO `persona` VALUES ('ADM01','Isaac Elias','Ñuflo Gamarra',19,'M','1997-06-21','4704444','peruano','jr. Carlos alayza y roel','soltero','seguridad informatica','75328398'),('MED02','AGÜERO CARHUAVILCA','JULIO CESAR',45,'M','0000-00-00','4756115','Peruano','av. 28 de Julio','viudo','----','75328398'),('MED03','ALATA GUTIERREZ','JOSE RODOLFO',25,'M','0000-00-00','4856213','Gringo','av. la molina','no sabe','----',''),('MED04','ALBAN MAZA','JOSUE SEBASTIAN',56,'M','0000-00-00','8465468','africano','av. tomas valle','casado','----',''),('MED05','ALVA PEREZ','CLAUDIA RAQUEL',32,'F','1984-03-25','','','','','',''),('MED06','ANTÚNEZ PALOMINO','KORI XIOMARA',21,'F','1995-04-08','','','','','',''),('MED07','APONTE FATAMA','RICARDO JR',25,'M','1991-06-17','','','','','',''),('MED08','AZORSA SALAZAR','STEPHANIE',54,'F','1962-06-15','','','','','',''),('MED09','BARRIENTOS MENDÍVIL','KEIBER MAGINS',28,'M','1988-01-19','','','','','',''),('MED10','BENITES PORRAS','JEAN CARLO',63,'M','1953-06-29','','','','','',''),('MED11','CARLOS','ÑUFLO',49,'M','0000-00-00','456123789','PERUANO','CARLOS ALAYZA','CASADO','null','45644564'),('MED12','JOSUE','MORENO WALDE',30,'M','1986-05-20','6188271','peruano','av.lima','soltero','medico','21902691'),('MED13','Kevin Luis','Monteza Corrales',21,'M','0000-00-00','973066207','Peruano','Ancón','Soltero','null','71501770'),('MED14','Martin Renato','Paz de la Cruz',35,'M','0000-00-00','956342518','Peruano','Av. Argentina 333','Viudo','null','56324589'),('MED15','CAMILA FABIOLA','ADRIAN CAMACHO',34,'F','1982-08-12','','peruana','','casada','medico','1233245'),('MED16',' LEONARDO','ADRIANO SABINO',40,'M','1980-12-12','123432','venezolano','av.La marina','casado','medico','08781234'),('MED17',' ANA PAULA','ALVARADO ANGULO',34,'F','1982-11-11','','colombiana','','casada','medico','06124513'),('MED18',' LADY FABIOLA','ALVARADO RAMOS',34,'F','1982-11-11','08123541','chileno','av.Tomas valle 1234','casada','medico','01928734'),('MED19','GUILLERMO MIGUEL','AMABLE VENTURA',40,'M','1989-12-12','01123412','PERUANO','','soltero','medico','07123554'),('null','Carlos','Ñuflo',49,'M','0000-00-00','123546789','Peruano','Carlos Alayza','Casado','null','45657895'),('PAC02','DANIEL OMAR','CÁCERES ARREDONDO',39,'M','1977-04-20','4564874','peruano','av. arequipa','soltero','estudiante','45615791'),('PAC03','DAVID HUMBERTO','CÁCERES CALLAÚ',41,'M','1975-03-12','4564876','peruano','av. los quipus','soltero','ingeniero',''),('PAC04','MARÍA DE JESÚS','CHALCO ARANIBAR',30,'F','1986-06-17','5463214','peruano','av. la marina','viudo','administrado',''),('PAC05','YERALDINY','COBEÑAS AVILA',29,'F','1987-03-18','','','','','',''),('PAC06','PAOLA MARINA','FARFAN VAZQUEZ',27,'F','1989-01-22','','','','','',''),('PAC11','ANA','GAMARRA',52,'F','1995-06-15','456159753','PERUANA','CARLOS ALAYZA','CASADA','OPERADORA','45568978'),('REC01','Michelle','Ramirez',20,'F','1995-10-17','4568134','peruano','Av Arenales','soltera','secretaria','75832389'),('REC2','Franco Samuel','Mecca Paredes',22,'M','1994-02-15','4727209','Peruano','SJM','Soltero','Estudiante','12354891'),('﻿MED01','ACOSTA HERRERA','VICTORIA GENOVEVA',23,'F','1993-04-03','','','','','',''),('﻿PAC01','CINTHYA PAOLA','ALVAREZ LOCK',34,'F','1982-05-10','','','','','','');
/*!40000 ALTER TABLE `persona` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-23 18:30:24
