package paneles;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import complementos.Conexion;

public class TablaCitas extends JPanel implements ActionListener{
	String CodigoMedico;
	JTable tabla;
	JScrollPane scroll;
	Vector<String[]> cache;
	JButton btn;
	JPanel sur;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public TablaCitas(String codigoMedico){
		this.CodigoMedico=codigoMedico;
		this.setLayout(new BorderLayout());
		refrescar();
		sur=new JPanel();
		btn=new JButton("Refrescar");
		btn.addActionListener(this);
		sur.add(btn);
		this.add(sur,BorderLayout.SOUTH);
	}
	@SuppressWarnings("deprecation")
	public void refrescar(){
		tabla=new JTable();
		String[] headers={"Nombre del Paciente","Hora de la cita"};
		cache=new Vector<String[]>();
		Connection cn=null;
		try{
			Date d=new Date();
			String dia= String.valueOf(1900+d.getYear())+"-"+String.valueOf(d.getMonth()+1)+"-"+d.getDate();
			cn=Conexion.getConexionMYSQL();
			Statement st= cn.createStatement();
			ResultSet rs=st.executeQuery("SELECT * FROM citas inner join persona"
					+ " on citas.codigoPaciente=persona.codigo where codigoMedico='"+CodigoMedico+"' and citas.fechaCita='"+dia+"';");
			while(rs.next()){
				String[] record= new String[2];
				record[0]=rs.getString("nombres")+rs.getString("apellidos");
				record[1]=rs.getTime("horaCita").toString();
				cache.addElement(record);
			}
			cn.close();
			st.close();
			rs.close();
			Object[][] o= cache.toArray(new Object[cache.size()][2]);
			getJTable(o,headers);
			scroll = new JScrollPane(tabla);
			this.add(scroll,BorderLayout.CENTER);
		}catch(Exception e){
			e.printStackTrace();
			JOptionPane.showMessageDialog(null,"Fallo en el llenado de tabla citas");
		}
	}
	private void getJTable(Object[][] data,String[] columnNames){
		DefaultTableModel model = new DefaultTableModel() {

		    /**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
		    public boolean isCellEditable(int row, int column) {
	        		return false;
		       //all cells false
		    }
		};
		TableRowSorter<DefaultTableModel> sorter = new TableRowSorter<DefaultTableModel>(model);
		model.setDataVector(data, columnNames);
		tabla.setModel(model);
		tabla.setRowSorter(sorter);
		tabla.setRowSelectionAllowed(false);
		tabla.getTableHeader().setReorderingAllowed(false);
}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource().equals(btn)){
			this.remove(scroll);
			refrescar();
			this.updateUI();
		}
	}

}
