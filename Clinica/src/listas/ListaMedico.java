package listas;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import complementos.Conexion;
import informacion.Medico;
import nodos.NodoMedico;

public class ListaMedico {

    private NodoMedico raiz;
    private int contador;

    public ListaMedico() {
        raiz = null;
        contador = 0;
        pushDataBase();
    }

    public void pushDataBase() {
        Medico medico = null;
        NodoMedico nodo = null;
        try {
            Connection cn = Conexion.getConexionMYSQL();
            Statement st = cn.createStatement();
            ResultSet rs1 = st.executeQuery("select * from medicos inner join persona on medicos.codigoMedico=persona.codigo;");
            while (rs1.next()) {
                medico = new Medico(rs1.getString("nombres"), rs1.getString("apellidos"), rs1.getInt("edad"), rs1.getString("sexo"), rs1.getString("telefono"), rs1.getString("nacionalidad"), rs1.getString("direccion"), rs1.getString("estadoCivil"), rs1.getString("dni"), rs1.getString("codigoMedico"),
                        rs1.getInt("numeroConsultorio"), rs1.getString("especialidad"), rs1.getString("dias"), rs1.getTime("horaInicio").toString(), rs1.getTime("horaFin").toString());
                nodo = new NodoMedico(medico);
                push(nodo);
            }
            rs1.close();
            st.close();
            cn.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void push(NodoMedico nodo) {
        if (raiz == null) {
            setRaiz(nodo);
            contador++;
        } else {
            NodoMedico aux = raiz;
            while (aux.getNext() != null) {
                aux = aux.getNext();
            }
            aux.setNext(nodo);
            contador++;
        }
    }

    public ArrayList<String> buscarEspecialidades() {
        NodoMedico aux = raiz;
        ArrayList<String> lista = new ArrayList<>();
        while (aux != null) {
            if (aux.getMedico().getEspecialidad() != null && !repetidos(aux.getMedico().getEspecialidad(), lista)) {
                lista.add(aux.getMedico().getEspecialidad());
            }
            aux = aux.getNext();
        }
        return lista;
    }

    public boolean repetidos(String es, ArrayList<String> lista) {
        boolean flag = false;
        for (String r : lista) {
        	flag=r.equals(es);
        }
        return flag;
    }

    public ArrayList<Medico> buscarMedicosPorEspecialidad(String especialidad) {
        ArrayList<Medico> lista = new ArrayList<>();
        NodoMedico aux = raiz;
        while (aux != null) {
            if (aux.getMedico().getEspecialidad().equals(especialidad)) {
                lista.add(aux.getMedico());
            }
            aux = aux.getNext();
        }
        return lista;
    }

    public Medico buscarMedico(String codigoMedico) {
        Medico med = new Medico();
        NodoMedico aux = raiz;
        while (aux != null) {
            if (aux.getMedico().getCodigoM().equals(codigoMedico)) {
                med = aux.getMedico();
            }
            aux = aux.getNext();
        }
        return med;
    }

    public NodoMedico getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoMedico raiz) {
        this.raiz = raiz;
    }

    public int getContador() {
        return contador;
    }

    public void setContador(int contador) {
        this.contador = contador;
    }

}
