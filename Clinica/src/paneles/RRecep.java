package paneles;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import com.toedter.calendar.JDateChooser;

import informacion.Recepcionista;

public class RRecep extends JPanel{
	private Recepcionista p;
	public RRecep() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{89, 122, 64, 86, 0};
		gridBagLayout.rowHeights = new int[]{20, 20, 20, 20, 20, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		JLabel lblNombre = new JLabel("Nombre:");
		GridBagConstraints gbc_lblNombre = new GridBagConstraints();
		gbc_lblNombre.anchor = GridBagConstraints.WEST;
		gbc_lblNombre.insets = new Insets(0, 0, 5, 5);
		gbc_lblNombre.gridx = 0;
		gbc_lblNombre.gridy = 0;
		add(lblNombre, gbc_lblNombre);
		
		textField_1 = new JTextField();
		GridBagConstraints gbc_textField_1 = new GridBagConstraints();
		gbc_textField_1.anchor = GridBagConstraints.WEST;
		gbc_textField_1.insets = new Insets(0, 0, 5, 5);
		gbc_textField_1.gridx = 1;
		gbc_textField_1.gridy = 0;
		add(textField_1, gbc_textField_1);
		textField_1.setColumns(10);
		
		rdbtnMasculino = new JRadioButton("Masculino");
		GridBagConstraints gbc_rdbtnMasculino = new GridBagConstraints();
		gbc_rdbtnMasculino.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnMasculino.gridx = 2;
		gbc_rdbtnMasculino.gridy = 0;
		add(rdbtnMasculino, gbc_rdbtnMasculino);
		
		rdbtnFemenino = new JRadioButton("Femenino");
		GridBagConstraints gbc_rdbtnFemenino = new GridBagConstraints();
		gbc_rdbtnFemenino.insets = new Insets(0, 0, 5, 0);
		gbc_rdbtnFemenino.gridx = 3;
		gbc_rdbtnFemenino.gridy = 0;
		add(rdbtnFemenino, gbc_rdbtnFemenino);
		
		JLabel lblApellido = new JLabel("Apellido:");
		GridBagConstraints gbc_lblApellido = new GridBagConstraints();
		gbc_lblApellido.anchor = GridBagConstraints.WEST;
		gbc_lblApellido.insets = new Insets(0, 0, 5, 5);
		gbc_lblApellido.gridx = 0;
		gbc_lblApellido.gridy = 1;
		add(lblApellido, gbc_lblApellido);
		
		textField = new JTextField();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.anchor = GridBagConstraints.NORTHWEST;
		gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.gridx = 1;
		gbc_textField.gridy = 1;
		add(textField, gbc_textField);
		textField.setColumns(10);
		
		JLabel lblNewLabel_6 = new JLabel("Dni:");
		GridBagConstraints gbc_lblNewLabel_6 = new GridBagConstraints();
		gbc_lblNewLabel_6.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblNewLabel_6.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_6.gridx = 2;
		gbc_lblNewLabel_6.gridy = 1;
		add(lblNewLabel_6, gbc_lblNewLabel_6);
		
		textField_4 = new JTextField();
		GridBagConstraints gbc_textField_4 = new GridBagConstraints();
		gbc_textField_4.anchor = GridBagConstraints.NORTHWEST;
		gbc_textField_4.insets = new Insets(0, 0, 5, 0);
		gbc_textField_4.gridx = 3;
		gbc_textField_4.gridy = 1;
		add(textField_4, gbc_textField_4);
		textField_4.setColumns(10);
		
		JLabel lblEdad = new JLabel("Edad:");
		GridBagConstraints gbc_lblEdad = new GridBagConstraints();
		gbc_lblEdad.anchor = GridBagConstraints.WEST;
		gbc_lblEdad.insets = new Insets(0, 0, 5, 5);
		gbc_lblEdad.gridx = 0;
		gbc_lblEdad.gridy = 2;
		add(lblEdad, gbc_lblEdad);
		
		textField_2 = new JTextField();
		GridBagConstraints gbc_textField_2 = new GridBagConstraints();
		gbc_textField_2.anchor = GridBagConstraints.NORTHWEST;
		gbc_textField_2.insets = new Insets(0, 0, 5, 5);
		gbc_textField_2.gridx = 1;
		gbc_textField_2.gridy = 2;
		add(textField_2, gbc_textField_2);
		textField_2.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Telefono:");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 2;
		gbc_lblNewLabel_1.gridy = 2;
		add(lblNewLabel_1, gbc_lblNewLabel_1);
		
		textField_5 = new JTextField();
		GridBagConstraints gbc_textField_5 = new GridBagConstraints();
		gbc_textField_5.anchor = GridBagConstraints.NORTHWEST;
		gbc_textField_5.insets = new Insets(0, 0, 5, 0);
		gbc_textField_5.gridx = 3;
		gbc_textField_5.gridy = 2;
		add(textField_5, gbc_textField_5);
		textField_5.setColumns(10);
		
		JLabel lblNewLabel_4 = new JLabel("Estado civil:");
		GridBagConstraints gbc_lblNewLabel_4 = new GridBagConstraints();
		gbc_lblNewLabel_4.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblNewLabel_4.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_4.gridx = 0;
		gbc_lblNewLabel_4.gridy = 3;
		add(lblNewLabel_4, gbc_lblNewLabel_4);
		
		textField_6 = new JTextField();
		GridBagConstraints gbc_textField_6 = new GridBagConstraints();
		gbc_textField_6.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_6.anchor = GridBagConstraints.NORTH;
		gbc_textField_6.insets = new Insets(0, 0, 5, 5);
		gbc_textField_6.gridx = 1;
		gbc_textField_6.gridy = 3;
		add(textField_6, gbc_textField_6);
		textField_6.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Nacionalidad:");
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2.gridx = 2;
		gbc_lblNewLabel_2.gridy = 3;
		add(lblNewLabel_2, gbc_lblNewLabel_2);
		
		textField_7 = new JTextField();
		GridBagConstraints gbc_textField_7 = new GridBagConstraints();
		gbc_textField_7.anchor = GridBagConstraints.NORTHWEST;
		gbc_textField_7.insets = new Insets(0, 0, 5, 0);
		gbc_textField_7.gridx = 3;
		gbc_textField_7.gridy = 3;
		add(textField_7, gbc_textField_7);
		textField_7.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("Direccion:");
		GridBagConstraints gbc_lblNewLabel_3 = new GridBagConstraints();
		gbc_lblNewLabel_3.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_3.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_3.gridx = 0;
		gbc_lblNewLabel_3.gridy = 4;
		add(lblNewLabel_3, gbc_lblNewLabel_3);
		
		textField_8 = new JTextField();
		GridBagConstraints gbc_textField_8 = new GridBagConstraints();
		gbc_textField_8.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_8.anchor = GridBagConstraints.NORTH;
		gbc_textField_8.insets = new Insets(0, 0, 5, 5);
		gbc_textField_8.gridx = 1;
		gbc_textField_8.gridy = 4;
		add(textField_8, gbc_textField_8);
		textField_8.setColumns(10);
		
		lblOcupacion = new JLabel("Ocupacion:");
		GridBagConstraints gbc_lblOcupacion = new GridBagConstraints();
		gbc_lblOcupacion.anchor = GridBagConstraints.WEST;
		gbc_lblOcupacion.insets = new Insets(0, 0, 5, 5);
		gbc_lblOcupacion.gridx = 2;
		gbc_lblOcupacion.gridy = 4;
		add(lblOcupacion, gbc_lblOcupacion);
		
		textField_9 = new JTextField();
		GridBagConstraints gbc_textField_9 = new GridBagConstraints();
		gbc_textField_9.insets = new Insets(0, 0, 5, 0);
		gbc_textField_9.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_9.gridx = 3;
		gbc_textField_9.gridy = 4;
		add(textField_9, gbc_textField_9);
		textField_9.setColumns(10);
		
		lblFecha = new JLabel("Fecha de nacimiento:");
		GridBagConstraints gbc_lblFecha = new GridBagConstraints();
		gbc_lblFecha.insets = new Insets(0, 0, 5, 5);
		gbc_lblFecha.gridx = 0;
		gbc_lblFecha.gridy = 5;
		add(lblFecha, gbc_lblFecha);
		
		dateChooser = new JDateChooser();
		dateChooser.setDateFormatString("yyyy-MM-dd");
		GridBagConstraints gbc_dateChooser = new GridBagConstraints();
		gbc_dateChooser.anchor = GridBagConstraints.WEST;
		gbc_dateChooser.insets = new Insets(0, 0, 5, 5);
		gbc_dateChooser.gridx = 1;
		gbc_dateChooser.gridy = 5;
		add(dateChooser, gbc_dateChooser);
		
		lblAreaDeAtencion = new JLabel("Area de Atencion:");
		GridBagConstraints gbc_lblAreaDeAtencion = new GridBagConstraints();
		gbc_lblAreaDeAtencion.anchor = GridBagConstraints.EAST;
		gbc_lblAreaDeAtencion.insets = new Insets(0, 0, 5, 5);
		gbc_lblAreaDeAtencion.gridx = 2;
		gbc_lblAreaDeAtencion.gridy = 5;
		add(lblAreaDeAtencion, gbc_lblAreaDeAtencion);
		
		textField_3 = new JTextField();
		GridBagConstraints gbc_textField_3 = new GridBagConstraints();
		gbc_textField_3.insets = new Insets(0, 0, 5, 0);
		gbc_textField_3.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_3.gridx = 3;
		gbc_textField_3.gridy = 5;
		add(textField_3, gbc_textField_3);
		textField_3.setColumns(10);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;
	private JLabel lblFecha;
	private JDateChooser dateChooser;
	private JLabel lblOcupacion;
	private JTextField textField_9;
	private JRadioButton rdbtnMasculino;
	private JRadioButton rdbtnFemenino;
	private JLabel lblAreaDeAtencion;
	private JTextField textField_3;
	public boolean isEmpty(){
		if(textField.getText().equals("") && textField_1.getText().equals("") && textField_2.getText().equals("") &&
			textField_4.getText().equals("") && textField_5.getText().equals("") && textField_6.getText().equals("") &&
			textField_7.getText().equals("") && textField_8.getText().equals("") && dateChooser.toString().equals("") &&
			!rdbtnMasculino.isSelected() && !rdbtnFemenino.isSelected() && textField_3.getText().equals("") ){
			return true;
		}
		else return false;
	}
	public void llenarDatos(){
		String sexo;
		if(rdbtnMasculino.isSelected()){
			sexo="M";
		}
		else{
			sexo="F";
		}
		p=new Recepcionista();
		p.setNombre(textField_1.getText());
		p.setApellido(textField.getText());
		p.setEdad(Integer.valueOf(textField_2.getText()));
		p.setSexo(sexo);
		p.setFechaNac(new java.sql.Date(dateChooser.getDate().getTime()));
		p.setTelefono(textField_5.getText());
		p.setNacionalidad(textField_7.getText());
		p.setDireccion(textField_8.getText());
		p.setEstadoCivil(textField_6.getText());
		p.setOcupacion(textField_9.getText());
		p.setDni(textField_4.getText());
		p.setAreaAtencion(textField_3.getText());
		p.crearCodigoRec();
	}
	public void nuevoIngreso(){
		textField_1.setText("");
		textField.setText("");
		textField_2.setText("");
		dateChooser.setCalendar(null);
		textField_5.setText("");
		textField_7.setText("");
		textField_8.setText("");
		textField_6.setText("");
		textField_9.setText("");
		textField_4.setText("");
		textField_3.setText("");
		p=null;
	}
	public Recepcionista getP() {
		return p;
	}
	public void setP(Recepcionista p) {
		this.p = p;
	}
}

