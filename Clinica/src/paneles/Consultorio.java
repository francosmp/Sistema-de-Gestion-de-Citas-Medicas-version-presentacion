package paneles;

import javax.swing.JPanel;
import java.awt.GridLayout;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import complementos.Conexion;

import java.awt.Color;

public class Consultorio extends JPanel implements Runnable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nombre;
	private String numeroConsultorio;
	private String codigoMedico;
	public boolean estado;
	public boolean estado1;
	JLabel lblNombre;
	JLabel lblNumero;
	Connection cn;
	JLabel lblEstado;
	public Consultorio(String nombre,String numeroConsultorio,String codigoMedico){
		this.nombre=nombre;
		this.numeroConsultorio=numeroConsultorio;
		this.codigoMedico=codigoMedico;
		cn=Conexion.getConexionMYSQL();
		GridLayout gridLayout = new GridLayout(3,1);
		setLayout(gridLayout);
		
		lblNombre = new JLabel("Nombre: "+nombre);
		lblNombre.setHorizontalAlignment(SwingConstants.LEFT);
		lblNombre.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		add(lblNombre);
		
		lblNumero= new JLabel("Numero de consultorio: "+numeroConsultorio);
		lblNumero.setHorizontalAlignment(SwingConstants.LEFT);
		lblNumero.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		add(lblNumero);
		
		lblEstado = new JLabel("Estado:");
		lblEstado.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		lblEstado.setHorizontalAlignment(SwingConstants.LEFT);
		add(lblEstado);
		estado=false;
		estado1=false;
	}
	private void verificarEstado() {
		// TODO Auto-generated method stub
		try{
			Statement st=cn.createStatement();
			ResultSet rs=st.executeQuery("SELECT * from temporal where codigoMedico='"+codigoMedico+"';");
			if(rs.next()){
				if(!estado1){
				lblEstado.setText("Estado: Paciente en atencion");
				lblEstado.setOpaque(true);
				lblEstado.setBackground(Color.green);
				lblEstado.updateUI();
				this.updateUI();
				}
				estado=false;
				estado1=true;
			}
			else{
				if(!estado){
				lblEstado.setText("Estado: No hay paciente");
				lblEstado.setOpaque(true);
				lblEstado.setBackground(Color.red);
				lblEstado.updateUI();
				this.updateUI();
				}
				estado=true;
				estado1=false;
			}
			st.close();
			rs.close();
		}catch(Exception ex){
			JOptionPane.showMessageDialog(null,"Fallo en verificacion de Estado");
		}
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		while(true){
			verificarEstado();
		}
	}

}
