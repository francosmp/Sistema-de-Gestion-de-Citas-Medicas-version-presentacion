package paneles;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import complementos.Conexion;

public class UpdateDatos extends JPanel implements ActionListener{
	public UpdateDatos(String acceso,String codigoCita) {
		this.codigoCita=codigoCita;
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 35, 236, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 70, 0, 70, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JLabel lblDiagnstico = new JLabel("Diagn\u00F3stico:");
		GridBagConstraints gbc_lblDiagnstico = new GridBagConstraints();
		gbc_lblDiagnstico.anchor = GridBagConstraints.WEST;
		gbc_lblDiagnstico.insets = new Insets(0, 0, 5, 0);
		gbc_lblDiagnstico.gridx = 3;
		gbc_lblDiagnstico.gridy = 2;
		add(lblDiagnstico, gbc_lblDiagnstico);
		
		textField = new JTextArea();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.insets = new Insets(0, 0, 5, 0);
		gbc_textField.fill = GridBagConstraints.BOTH;
		gbc_textField.gridx = 3;
		gbc_textField.gridy = 3;
		add(textField, gbc_textField);
		textField.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Receta:");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel.gridx = 3;
		gbc_lblNewLabel.gridy = 4;
		add(lblNewLabel, gbc_lblNewLabel);
		
		textField_1 = new JTextArea();
		GridBagConstraints gbc_textField_1 = new GridBagConstraints();
		gbc_textField_1.insets = new Insets(0, 0, 5, 0);
		gbc_textField_1.fill = GridBagConstraints.BOTH;
		gbc_textField_1.gridx = 3;
		gbc_textField_1.gridy = 5;
		add(textField_1, gbc_textField_1);
		textField_1.setColumns(10);
		
		btnNewButton = new JButton("Actualizar");
		btnNewButton.addActionListener(this);
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.anchor = GridBagConstraints.EAST;
		gbc_btnNewButton.gridx = 3;
		gbc_btnNewButton.gridy = 6;
		add(btnNewButton, gbc_btnNewButton);
		Connection cn=Conexion.getConexionMYSQL();
		
		try{
			Statement st=cn.createStatement();
			ResultSet rs=st.executeQuery("select * from historia inner join citas on historia.codigoCita=citas.codigoCita"
					+ " where citas.acceso='"+acceso+"' and citas.codigoCita='"+codigoCita+"';");
			if(rs.next()){
				textField.setText(rs.getString("diagnostico"));
				textField_1.setText(rs.getString("receta"));
			}
			cn.close();
			st.close();
			rs.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextArea textField;
	private JTextArea textField_1;
	private JButton btnNewButton;
	private String codigoCita;
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource().equals(btnNewButton)){
			Connection cn=Conexion.getConexionMYSQL();
			try{
				Statement st=cn.createStatement();
				st.executeUpdate("UPDATE historia SET historia.diagnostico='"+textField.getText()
				+"' , historia.receta='"+textField_1.getText()+"' where historia.codigoCita='"+codigoCita+"';");
				st.executeUpdate("update citas set permiso='0' where codigoCita='"+codigoCita+"';");
				cn.close();
				st.close();
				JOptionPane.showMessageDialog(null,"Datos Actualizados");
				this.removeAll();
				this.updateUI();
				Modificar.textField.setText("");
				Modificar.textField_1.setText("");
			}catch(Exception ex){
				ex.printStackTrace();
				JOptionPane.showMessageDialog(null,"Error de Conexion");
			}
		}
	}

}
