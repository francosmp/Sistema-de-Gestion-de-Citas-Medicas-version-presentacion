package informacion;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import complementos.Conexion;

public class Medico extends Persona{
	private String codigo;
	private int numConsultorio;
	private String especialidad;
	public Medico(){}
	public String getHoraInicio() {
		return horaInicio;
	}
	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}
	public String getHoraFin() {
		return horaFin;
	}
	public void setHoraFin(String horaFin) {
		this.horaFin = horaFin;
	}
	public void crearCodigoMed(){
		Connection cn=Conexion.getConexionMYSQL();
		int contador=0;
		try {
			Statement st=cn.createStatement();
			ResultSet rs=st.executeQuery("SELECT codigo FROM persona where persona.codigo like '%MED%'");
			while(rs.next()){
				contador++;
			}
		String codigo="MED"+String.valueOf(contador+1);
		this.codigo=codigo;
		cn.close();
		st.close();
		rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private String dias;
	private String horaInicio;
	private String horaFin;
	public Medico(String nombre, String apellido, int edad, String sexo, String telefono,
			String nacionalidad, String direccion, String estadoCivil,String dni, String codigo,
			int numConsultorio, String especialidad,String dias,String horaInicio,String horaFin) {
		super(nombre, apellido, edad, sexo, telefono, nacionalidad, direccion, estadoCivil,dni);
		this.codigo = codigo;
		this.numConsultorio = numConsultorio;
		this.especialidad = especialidad;
		this.dias=dias;
		this.horaFin=horaFin;
		this.horaInicio=horaInicio;
	}
	public String getCodigoM() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public int getNumConsultorio() {
		return numConsultorio;
	}
	public void setNumConsultorio(int numConsultorio) {
		this.numConsultorio = numConsultorio;
	}
	public String getEspecialidad() {
		return especialidad;
	}
	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}
	public String getDias() {
		return dias;
	}
	public void setDias(String dias) {
		this.dias = dias;
	}
}

