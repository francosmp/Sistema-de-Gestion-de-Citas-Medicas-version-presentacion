package nodos;

import informacion.Medico;

public class NodoMedico {
	private Medico medico;
	private NodoMedico next;
	public NodoMedico(Medico m){
		medico=m;
		next=null;
	}
	public Medico getMedico() {
		return medico;
	}
	public void setMedico(Medico medico) {
		this.medico = medico;
	}
	public NodoMedico getNext() {
		return next;
	}
	public void setNext(NodoMedico next) {
		this.next = next;
	}
}
