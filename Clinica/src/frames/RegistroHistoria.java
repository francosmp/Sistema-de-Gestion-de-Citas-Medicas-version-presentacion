package frames;

import complementos.Conexion;
import complementos.MiFrame;
import paneles.Modificar;

import java.awt.GridBagLayout;
import javax.swing.JTextArea;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.Statement;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;

public class RegistroHistoria extends MiFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String codigoCita;
	JTextArea textArea;
	JTextArea textArea_1;
	JTextArea textArea_2;
	JButton btnRegistrarHistoria;
	public RegistroHistoria(String codigoCita){
		super("Registrar Historia");
		this.codigoCita=codigoCita;
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 67, 120, 46, 0};
		gridBagLayout.rowHeights = new int[]{0, 90, 0, 90, 0, 90, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		getContentPane().setLayout(gridBagLayout);
		
		JLabel lblDescripcin = new JLabel("Descripci\u00F3n:");
		GridBagConstraints gbc_lblDescripcin = new GridBagConstraints();
		gbc_lblDescripcin.insets = new Insets(0, 0, 5, 5);
		gbc_lblDescripcin.gridx = 3;
		gbc_lblDescripcin.gridy = 0;
		getContentPane().add(lblDescripcin, gbc_lblDescripcin);
		
		textArea = new JTextArea();
		GridBagConstraints gbc_textArea = new GridBagConstraints();
		gbc_textArea.insets = new Insets(0, 0, 5, 0);
		gbc_textArea.gridwidth = 3;
		gbc_textArea.fill = GridBagConstraints.BOTH;
		gbc_textArea.gridx = 3;
		gbc_textArea.gridy = 1;
		getContentPane().add(textArea, gbc_textArea);
		
		JLabel lblDiagnostico = new JLabel("Diagnostico:");
		GridBagConstraints gbc_lblDiagnostico = new GridBagConstraints();
		gbc_lblDiagnostico.insets = new Insets(0, 0, 5, 5);
		gbc_lblDiagnostico.gridx = 3;
		gbc_lblDiagnostico.gridy = 2;
		getContentPane().add(lblDiagnostico, gbc_lblDiagnostico);
		
		textArea_1 = new JTextArea();
		GridBagConstraints gbc_textArea_1 = new GridBagConstraints();
		gbc_textArea_1.insets = new Insets(0, 0, 5, 0);
		gbc_textArea_1.gridwidth = 3;
		gbc_textArea_1.fill = GridBagConstraints.BOTH;
		gbc_textArea_1.gridx = 3;
		gbc_textArea_1.gridy = 3;
		getContentPane().add(textArea_1, gbc_textArea_1);
		
		JLabel lblReceta = new JLabel("Receta:");
		GridBagConstraints gbc_lblReceta = new GridBagConstraints();
		gbc_lblReceta.insets = new Insets(0, 0, 5, 5);
		gbc_lblReceta.gridx = 3;
		gbc_lblReceta.gridy = 4;
		getContentPane().add(lblReceta, gbc_lblReceta);
		
		textArea_2 = new JTextArea();
		GridBagConstraints gbc_textArea_2 = new GridBagConstraints();
		gbc_textArea_2.insets = new Insets(0, 0, 5, 0);
		gbc_textArea_2.gridwidth = 3;
		gbc_textArea_2.fill = GridBagConstraints.BOTH;
		gbc_textArea_2.gridx = 3;
		gbc_textArea_2.gridy = 5;
		getContentPane().add(textArea_2, gbc_textArea_2);
		
		btnRegistrarHistoria = new JButton("Registrar Historia");
		btnRegistrarHistoria.addActionListener(this);
		GridBagConstraints gbc_btnRegistrarHistoria = new GridBagConstraints();
		gbc_btnRegistrarHistoria.insets = new Insets(0, 0, 0, 5);
		gbc_btnRegistrarHistoria.gridx = 4;
		gbc_btnRegistrarHistoria.gridy = 7;
		getContentPane().add(btnRegistrarHistoria, gbc_btnRegistrarHistoria);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setResizable(false);
		this.pack();
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource().equals(btnRegistrarHistoria)){
			Connection cn=Conexion.getConexionMYSQL();
			try{
				Statement st=cn.createStatement();
				st.executeUpdate("UPDATE historia SET historia.diagnostico='"+textArea_1.getText()
				+"' , historia.receta='"+textArea_2.getText()+"' , historia.descripcion='"+textArea.getText()+"' where historia.codigoCita='"+codigoCita+"';");
				cn.close();
				st.close();
				JOptionPane.showMessageDialog(null,"Datos registrados");
				this.dispose();
			}catch(Exception ex){
				ex.printStackTrace();
				JOptionPane.showMessageDialog(null,"Fallo en registrar el historial");
			}
		}
	}

}
