package complementos;

import informacion.Cita;
import informacion.Historia;
import informacion.Persona;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

public class registrarBasedatos {

    public boolean registrarPaciente(Persona p, String sangre, String peso, String talla, String alergias) {
        Connection conectar = Conexion.getConexionMYSQL();
        boolean estado;
        if (p != null) {
            try {
                PreparedStatement ps = conectar.prepareStatement("insert into persona values(?,?,?,?,?,?,?,?,?,?,?,?)");
                ps.setString(1, p.getCodigop());
                ps.setString(2, p.getNombre());
                ps.setString(3, p.getApellido());
                ps.setInt(4, p.getEdad());
                ps.setString(5, p.getSexo());
                ps.setDate(6, p.getFechaNac());
                ps.setString(7, p.getTelefono());
                ps.setString(8, p.getNacionalidad());
                ps.setString(9, p.getDireccion());
                ps.setString(10, p.getEstadoCivil());
                ps.setString(11, p.getOcupacion());
                ps.setString(12, p.getDni());
                ps.executeUpdate();
                conectar.close();
                ps.close();
                crearHistoriales(sangre, talla, peso, p.getCodigop(), alergias);
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                JOptionPane.showMessageDialog(null, "Fallo en registrar paciente");
            }
            return estado = true;
        } else {
            System.out.println("lista vacia");
            estado = false;
        }
        return estado;
    }

    private void crearHistoriales(String sangre, String talla, String peso, String codigoP, String alergias) {
        Connection conectar = Conexion.getConexionMYSQL();
        Historia h = new Historia();
        String codigoH = h.crearCodigo();
        try {
            PreparedStatement ps = conectar.prepareStatement("INSERT INTO historiales values(?,?,?,?,?,?)");
            ps.setString(1, codigoP);
            ps.setString(2, codigoH);
            ps.setDouble(3, Double.valueOf(peso));
            ps.setDouble(4, Double.valueOf(talla));
            ps.setString(5, sangre);
            ps.setString(6, alergias);
            ps.executeUpdate();
            ps.close();
            conectar.close();

        } catch (SQLException e) {
            System.out.println(e.getMessage() + "crearHistorial");
            JOptionPane.showMessageDialog(null, "Fallo crear Historiales");
        }
    }

    public boolean RegistrarCita(Cita cita) {
        Connection conectar = Conexion.getConexionMYSQL();
        boolean estado;
        if (cita != null) {
            try {
                PreparedStatement ps = conectar.prepareStatement("insert into citas values(?,?,?,?,?,?,?,?)");
                ps.setString(1, cita.getCodigo());
                ps.setString(2, cita.getPaciente().getCodigop());
                ps.setString(3, cita.getMedico().getCodigoM());
                ps.setDate(4, cita.getFechaCita());
                ps.setTime(5, cita.getHoraCita());
                ps.setString(6, cita.getAcceso());
                ps.setString(7, cita.getDescripcion());
                ps.setInt(8, 0);
                ps.executeUpdate();
                crearHistoria(cita);
                conectar.close();
                ps.close();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Fallo en registrar cita");
            }
            estado = true;
        } else {
            System.out.println("error vacia");
            estado = false;
        }
        return estado;
    }

    private String buscarCodigoHistorial(Cita cita) {
        String codigo = null;
        Connection cn = Conexion.getConexionMYSQL();
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery("SELECT codigoHistorial FROM historiales where historiales.codigoPaciente='" + cita.getPaciente().getCodigop() + "';");
            if (rs.next()) {
                codigo = rs.getString("codigoHistorial");
            }
            cn.close();
            st.close();
            rs.close();
            return codigo;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            System.out.println(e.getMessage());
            JOptionPane.showMessageDialog(null, "fallo en buscar codigo historial");
            return codigo;
        }
    }

    private void crearHistoria(Cita cita) {
        // TODO Auto-generated method stub
        String codigo = null;
        Connection conectar = Conexion.getConexionMYSQL();
        codigo = buscarCodigoHistorial(cita);
        try {
            PreparedStatement ps = conectar.prepareStatement("insert into historia values(?,?,?,?,?)");
            ps.setString(1, codigo);
            ps.setString(2, "-----");
            ps.setString(3, "-----");
            ps.setString(4, cita.getCodigo());
            ps.setString(5, "-----");
            ps.executeUpdate();
            conectar.close();
            ps.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            JOptionPane.showMessageDialog(null, "Fallo en crear historia");
        }
    }

    public void eliminarCita(String codigo, String codigoM, Date fechaC, String codigoH) {
        Connection conectar = Conexion.getConexionMYSQL();
        String cadena = "Cita cancelada";
        try {
            PreparedStatement ps = conectar.prepareStatement("update citas set citas.descripcion=? where citas.codigoCita=?");
            ps.setString(1, cadena);
            ps.setString(2, codigo);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Fallo en eliminar cita");
        }
        try {
            PreparedStatement ps = conectar.prepareStatement("update historia set historia.descripcion=? where historia.codigoCita=?");
            ps.setString(1, cadena);
            ps.setString(2, codigo);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Fallo al actualizar historia");
        }
        try {
            PreparedStatement pss = conectar.prepareStatement("delete from turnos where turnos.codigoMedico=? and turnos.codigoHora=? and turnos.dia=?");
            pss.setString(1, codigoM);
            pss.setString(2, codigoH);
            pss.setString(3, fechaC.toString());
            pss.executeUpdate();
            pss.close();
            conectar.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Fallo en eliminar turno");
        }

    }

    public boolean RegistrarTurno(String CodigoM, String codigoH, Date FechaCita) {
        Connection conectar = Conexion.getConexionMYSQL();
        boolean update = false;
        int id = buscarId();
        try {
            PreparedStatement ps = conectar.prepareStatement("insert into turnos values(?,?,?,?)");
            ps.setString(1, CodigoM);
            ps.setString(2, codigoH);
            ps.setInt(3, id);
            ps.setDate(4, FechaCita);
            update = ps.executeUpdate() > 0;
            conectar.close();
            ps.close();
            return update;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            JOptionPane.showMessageDialog(null, "Fallo en registrar turno");
            return update;
        }
    }

    private int buscarId() {
        Connection conectar = Conexion.getConexionMYSQL();
        int cont = 0;
        try {
            PreparedStatement ps = conectar.prepareStatement("SELECT ID FROM turnos;");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                cont++;
            }
            return cont + 1;

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            JOptionPane.showMessageDialog(null, "Fallo buscar ID");
           
            return -1;
        }

    }

    public boolean verificarHorario(String codigo, Date fec, String codigoH) {
        String sql = "select count(*) from turnos where (turnos.codigoMedico=" + "'" + codigo + "'" + " and turnos.dia=" + "'" + fec + "'" + " and turnos.codigoHora=" + "'" + codigoH + "')";
        Connection conectar = Conexion.getConexionMYSQL();
        try {
            Statement st = conectar.createStatement();
            ResultSet rs = st.executeQuery(sql);
            rs.next();
            int n = rs.getInt("count(*)");
            conectar.close();
            st.close();
            rs.close();
            return n > 0;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Fallo en verificar horario");
            return false;
        }
    }

    public boolean verificarFecha(String codigo, Date fecha, int horasT) {
        String sql = "select count(*) from turnos where (turnos.codigoMedico=" + "'" + codigo + "'" + " and turnos.dia=" + "'" + fecha + "')";
        Connection conectar = Conexion.getConexionMYSQL();
        try {
            Statement st = conectar.createStatement();
            ResultSet rs = st.executeQuery(sql);
            rs.next();
            int n = rs.getInt("count(*)");
            conectar.close();
            st.close();
            rs.close();
            return n > horasT;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Fallo en verificar fecha");
            return false;
        }
    }
}
